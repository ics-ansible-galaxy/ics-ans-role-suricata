# ics-ans-role-suricata

Ansible role to install [suricata](https://suricata.readthedocs.io/en/suricata-4.1.2/what-is-suricata.html).
The default settings work. Suricata will log via syslog.
Exporting logs to a central syslog server and/or to influxdb (ELK or Graylog) can be achieved with the [ics-ans-role-syslog-client](https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-syslog-client?nav_source=navbar) role.

Due to epel-release not having the suricata 4.1+, suricata-update is not included and needs to be installed as a standalone. (Can be removed when epel-release updates its package)

# Set up

Rules included in the file folder is optional; to load them configure update.yaml for local files. Emerging rulesets are not loaded to suricata due to suricata-update using it's default source in update.yaml to fetch version appropiate emerging rulesets instead.

Disable filter will run first; if nothing is included it will run every rule that's been downloaded to suricata-update from source.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables
```yaml
# corporate http proxy
proxy: proxyserver.domain:8000
# suricata.yaml
# possible runmodes are worker, autofp or single for development
runmode: autofp # HIDS or virtual machine
# runmode: worker # Physical machine or when virtual NIC is set to virtio
# runmode: single # testing or development
# This setting controls the number simultaneous packets that the engine can handle. Setting this higher generally keeps the threads more busy, but setting it too high will lead to degradation.
max_pend_pkts: 1024

# detection engine inspection rules for signatures that traffic will be compared against.
# Higher values means higher performance (more memory requirements).
toclient: 25
toserver: 25

# Set this to snoop an interface as well as runmode to worker
suricata_interface: eth0

# Set this to internale networks
suricata_networks: 172.16.0.0/16,192.168.0.0/16

# declare proxy server and port format: nn.nn.nn.nn:pp
suricata_proxy:

# enable of disable suricata feed updates
suricata_update_enabled: true

# list of suricata source feeds
suricata_update_sources: []
 - oisf/trafficid
 - malsilo/win-malware
 - et/open
 - ptresearch/attackdetection
 - tgreen/hunting

# list of rules to disable
suricata_disable_rules: []
 - 2210000
 - 2210001
 - 2210002

...
```

## Example Playbook

```yaml
- hosts: servers
  become: true
  roles:
    - role: ics-ans-role-suricata
```

## License

BSD 2-clause
