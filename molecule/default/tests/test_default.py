import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_suricata_is_installed(host):
    assert host.package("suricata").is_installed


def test_suricata_is_running_and_enabled(host):
    suricata = host.service("suricata")
    assert suricata.is_enabled
    assert suricata.is_running


def test_suricata_file(host):
    suricata = host.file("/etc/suricata/suricata.yaml")
    assert suricata.exists
    assert suricata.is_file
